<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Rel_com_emp;
use Illuminate\Support\Facades\DB;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($index=0)
    {
        if ($index === 0 ){
            $result = DB::select('select e.id,e.name,e.email, c.name as company from employees e
            inner join rel_com_emps r on e.id = r.employee_id
            inner join companies c on r.company_id = c.id
            ');

        }else {
            $result = DB::select('select e.id,e.name,e.email, c.name as company from employees e
            inner join rel_com_emps r on e.id = r.employee_id
            inner join companies c on r.company_id = c.id
            where e.id = ?', [$index]);

        }
        return  $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $emp = new Employee();
        $emp->name = $request->name;
        $emp->email = $request->email;
        $emp->save();
        
        $rel = new Rel_com_emp();
        $rel->employee_id = $emp->id;
        $rel->company_id = $request->company_id;
        $rel->save();

        return EmployeesController::index($emp->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emp = Employee::find($id);
        $emp->name = $request->name;
        $emp->email = $request->email;
        $emp->save();
        return $emp;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $rel_id = Rel_com_emp::where('employee_id',$id)->get();
        
        $rel = Rel_com_emp::find($rel_id[0]['id']);
        $rel->delete();

        $emp = Employee::find($id);
        $emp->delete();


    }
}
