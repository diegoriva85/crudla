<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'userTest',
            'email'=>'userTest@test.com',
            'password'=>bcryp('qwerty123'),
        ]);
    }
}
