## Aplicación CRUD en Laravel con Vuejs

### instalacion:

Descargar el proyecto 

ejecutar ```composer install``` para installar las dependencias de laravel
ejecutar ```npm install```  para instalar las dependendcias de Vuejs

### Definición de archivos:
/.env se encuentra la configuracion de acceso a la base local
El archivo Dump-crudla.sql con el dump de la base en la carpeta crudla/database esta 

### Ejemplo de ejecucion:
```php artisan serve``` para correr el servidor de laravel
```npm run watch``` para correr el servidor de Vue 